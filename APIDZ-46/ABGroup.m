//
//  ABGroup.m
//  APIDZ-46
//
//  Created by tarik on 17.09.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABGroup.h"

@implementation ABGroup

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super initWithServerResponse:responseObject];
    if (self) {
        
        self.groupName = [responseObject objectForKey:@"name"];
        
        NSString *photoMediumURL = [responseObject objectForKey:@"photo_medium"];
        
        if (photoMediumURL) {
            self.photoMediumURL = [NSURL URLWithString:photoMediumURL];
        }
        
    }
    return self;
}


@end
