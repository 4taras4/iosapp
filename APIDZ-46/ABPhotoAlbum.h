//
//  ABPhotoAlbum.h
//  APIDZ-46
//
//  Created by tarik on 06.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABServerObject.h"

@interface ABPhotoAlbum : ABServerObject

@property (strong, nonatomic) NSString *titlePhotosAlbum;
@property (assign, nonatomic) NSInteger sizePhotos;
@property (strong, nonatomic) NSString *dateUpdated;
@property (strong, nonatomic) NSString *albumId;


@end
