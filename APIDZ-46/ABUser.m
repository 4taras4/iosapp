//
//  ABUser.m
//  APIDZ-46
//
//  Created by tarik on 27.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABUser.h"

@implementation ABUser

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super initWithServerResponse:responseObject];
    if (self) {
        self.firstName = [responseObject objectForKey:@"first_name"];
        self.lastName = [responseObject objectForKey:@"last_name"];
        
        NSString *urlStringphoto50 = [responseObject objectForKey:@"photo_50"];
        
        if (urlStringphoto50) {
            self.imageURL = [NSURL URLWithString:urlStringphoto50];
        }
        
        NSString *photo = [responseObject objectForKey:@"photo"];
        
        if (photo) {

            self.photo = [NSURL URLWithString:photo];
        }
        
        NSString *urlStringPhotoMedium = [responseObject objectForKey:@"photo_medium_rec"];
        
        if (urlStringPhotoMedium) {
            self.photoMediumURL = [NSURL URLWithString:urlStringPhotoMedium];
        }
    }
    return self;
}


@end
