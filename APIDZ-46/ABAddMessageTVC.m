//
//  ABAddMessageTVC.m
//  APIDZ-46
//
//  Created by tarik on 06.09.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABAddMessageTVC.h"
#import "JSQMessagesCollectionView.h"
#import "ABServerManager.h"
#import "ABWall.h"
#import "ABItemMessage.h"
#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>



@interface ABAddMessageTVC ()

@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

@property (strong, nonatomic) NSOperation *currentOperation;
@property (strong, nonatomic) NSOperationQueue *queue;
@property (strong, nonatomic) UIImage *imageAvatarIncoming;
@property (strong, nonatomic) UIImage *imageAvatarOutgoing;

@end

@implementation ABAddMessageTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIRefreshControl *refreshControlBottom = [[UIRefreshControl alloc] init];
    refreshControlBottom.triggerVerticalOffset = 100.;
    [refreshControlBottom addTarget:self action:@selector(refreshBottom:) forControlEvents:UIControlEventValueChanged];
    
    //[self.collectionView addSubview:refreshControl];
    
    self.collectionView.bottomRefreshControl = refreshControlBottom;
    
    UIRefreshControl *refreshControlTop = [[UIRefreshControl alloc] init];
    //refreshControlTop.triggerVerticalOffset = 100.;
    [refreshControlTop addTarget:self action:@selector(refreshTop:) forControlEvents:UIControlEventValueChanged];
    
    [self.collectionView addSubview:refreshControlTop];
    
    //self.collectionView.bottomRefreshControl = refreshControlBottom;
    
    self.navigationItem.title = self.senderDisplayName;
    
    self.messages = [NSMutableArray array];
    
    self.imageAvatarOutgoing = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.avatarOutgoing]];
    self.imageAvatarIncoming= [UIImage imageWithData:[NSData dataWithContentsOfURL:self.avatarIncoming]];
    
    //[self getHistoryMessagesFromServer:20 offset:0];
    [self getHistoryMessagesBackgroundFromServer:20 offset:0];
    
    //Color the inputview background
    
    //Delete the avatars appearing next to the messages
    //self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    //self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
   // self.collectionView.collectionViewLayout.
    
    
    //self.showLoadEarlierMessagesHeader = YES;
    /*
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage jsq_defaultTypingIndicatorImage] style:UIBarButtonItemStylePlain target:self action:@selector(receiveMessagePressed:)];
    */
    /**
     *  Customize your toolbar buttons
     *
     *  self.inputToolbar.contentView.leftBarButtonItem = custom button or nil to remove
     *  self.inputToolbar.contentView.rightBarButtonItem = custom button or nil to remove
     */
    
    JSQMessagesBubbleImageFactory *bubbleFactory= [[JSQMessagesBubbleImageFactory alloc] init];
    
    self.outgoingBubbleImageData=[bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    self.incomingBubbleImageData=[bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
    
    
    //[self.collectionView reloadData];

}

- (void)refreshBottom:(UIRefreshControl *)refreshControl {
    
    //[self.messages removeAllObjects];
    
    [self updateMessages];
    
    [refreshControl endRefreshing];
}

- (void)refreshTop:(UIRefreshControl *)refreshControl {
    
    [self getHistoryMessagesBackgroundFromServer:20 offset:[self.messages count]];
    
    [refreshControl endRefreshing];
}



- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    self.collectionView.collectionViewLayout.springinessEnabled = YES;
    
    //[self scrollToBottomAnimated:NO];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Send Button

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date {
    /*
    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
                                             senderDisplayName:self.senderDisplayName
                                                          date:[NSDate date]
                                                          text:text];
    
    [self.messages addObject:message];
    */
    [self didSendMessageWithText:text];
    
    //[self finishSendingMessageAnimated:YES];
}
   
#pragma mark - JSQMessages Data Source methods

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    //Return the actual message at each indexpath.row
    return [self.messages objectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    if (![self incoming:message]) {
        
        return self.outgoingBubbleImageData;
        
    } else {
        
        return self.incomingBubbleImageData;
    }
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView
                    avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    UIImage *image = nil;
    
    if (![self incoming:message]) {
        
        image = self.imageAvatarIncoming;
        
    } else {
        
        image = self.imageAvatarOutgoing;
    }
    
    JSQMessagesAvatarImage *avatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:image diameter:20];
    return avatar;
}
/*
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
 
     //  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     //  The other label text delegate methods should follow a similar pattern.
     //
     //  Show a timestamp for every 3rd message
 
    //if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
    //cell.cellBottomLabel.text = [dateFormatter stringFromDate:message.date];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[dateFormatter stringFromDate:message.date]];
    return attributedString;
    //}
    
    //return nil;
}
*/

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
    //cell.cellBottomLabel.text = [dateFormatter stringFromDate:message.date];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[dateFormatter stringFromDate:message.date]];
    return attributedString;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    JSQMessage *message = self.messages[indexPath.item];
    
    if ([self incoming:message]) {
        return nil;
    }
    if (indexPath.item - 1 > 0) {
        
        JSQMessage *previous = self.messages[indexPath.item - 1];
        if ([previous.senderId isEqualToString:message.senderId]) {
            
                return nil;
            }
        }
        return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

#pragma mark - Collection View

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return [self.messages count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    if (!message.isMediaMessage) {
        
        if (![self incoming:message]) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleThick | NSUnderlinePatternSolid)};
    }
    
    return cell;
    
}

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    //if (indexPath.item % 3 == 0) {
        //return kJSQMessagesCollectionViewCellLabelHeightDefault;
    //}
    
     return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    
   
     //iOS7-style sender name labels
     
    JSQMessage *currentMessage = [self.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return kJSQMessagesCollectionViewAvatarSizeDefault;
}


- (void)getHistoryMessagesBackgroundFromServer:(NSInteger)count offset:(NSInteger)offset {
    
    self.queue = [[NSOperationQueue alloc] init];
    
    __weak NSOperation *weakCurrentOperation = self.currentOperation;
    
    __weak ABAddMessageTVC* weakSelf = self;
    
    self.currentOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        if (![weakCurrentOperation isCancelled]) {
            
            [weakSelf getHistoryMessagesFromServer:count offset:offset];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [weakSelf.collectionView reloadData];
            
            self.currentOperation = nil;
        });
        
    }];
    
    [self.queue addOperation:self.currentOperation];
}


- (void)getHistoryMessagesFromServer:(NSInteger)count offset:(NSInteger)offset {
    
    [[ABServerManager shareManager] getHistoryMessagesFromUserId:self.senderId senderName:self.senderDisplayName count:count offset:offset onSuccess:^(NSArray *array) {
        
        [self.messages addObjectsFromArray:array];
        
        NSSortDescriptor *date = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];

        [self.messages sortUsingDescriptors:[NSArray arrayWithObject:date]];
        
        //NSLog(@"messages count = %ld", (unsigned long)[self.messages count]);
        
        [self.collectionView reloadData];
        
        [self scrollToBottomAnimated:YES];
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

- (void)sendMessage:(NSString *)message {
    
    [[ABServerManager shareManager] sendMessageForUserId:self.senderId message:message onSuccess:^(id result) {
        
        [self finishSendingMessageAnimated:YES];
        
        [self getHistoryMessagesBackgroundFromServer:10 offset:0];
        
        //[self.collectionView reloadData];
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

-(void)didSendMessageWithText:(NSString*)text{
   // AVIMTextMessage *message=[AVIMTextMessage messageWithText:text attributes:nil];
    
    NSString *message = [NSString stringWithFormat:@"%@", text];
    
    [self sendMessage:message];
}

- (void)didPressAccessoryButton:(UIButton *)sender {
    /*
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Media messages"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"Send photo", @"Send location", @"Send video", nil];
    
    [sheet showFromToolbar:self.inputToolbar];
     */
}

///Users/Aleksandr/Documents/developer/APIDZ-46/APIDZ-46/ABAddMessageTVC.m:403:44: 'UIActionSheet' is deprecated: first deprecated in iOS 8.3 - UIActionSheet is deprecated. Use UIAlertController with a preferredStyle of UIAlertControllerStyleActionSheet instead

- (IBAction)cancelAction:(UIBarButtonItem *)sender {
    
    //[self.messages removeAllObjects];
    
    //[self.collectionView reloadData];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

/*
#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    [self getHistoryMessagesFromServer];
}

*/

#pragma mark - ABAddPostDelegate

- (void)updateMessages {
    
    [[ABServerManager shareManager] getHistoryMessagesFromUserId:self.senderId senderName:self.senderDisplayName count:MAX(10, [self.messages count]) offset:0 onSuccess:^(NSArray *array) {
        
        [self.messages removeAllObjects];
        
        [self.messages addObjectsFromArray:array];
        
        NSSortDescriptor *date = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
        
        [self.messages sortUsingDescriptors:[NSArray arrayWithObject:date]];
        
        [self.collectionView reloadData];
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

#pragma mark - Other methods

- (BOOL)incoming:(JSQMessage *)message {
    
    return ([message.senderId isEqualToString:self.senderId] == NO);
}




@end
