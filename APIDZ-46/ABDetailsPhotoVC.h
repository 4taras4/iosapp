//
//  ABDetailsPhotoVC.h
//  APIDZ-46
//
//  Created by tarik on 07.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABPhoto;

@interface ABDetailsPhotoVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (strong, nonatomic) ABPhoto *photo;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *likesLabel;
@property (weak, nonatomic) IBOutlet UIImageView *lilesImageView;

- (IBAction)backAction:(UIBarButtonItem *)sender;

@end
