//
//  ABPhotoCVC.m
//  APIDZ-46
//
//  Created by tarik on 06.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABPhotoCVC.h"
#import "ABPhoto.h"
#import "ABPhotoCell.h"
#import "UIImageView+AFNetworking.h"
#import "ABServerManager.h"
#import "ABPhotoAlbum.h"
#import "ABDetailsPhotoVC.h"
#import "ABCameraVC.h"
#import "Masonry.h"

@interface ABPhotoCVC () <ABCameraDelegate>

@property (strong, nonatomic) NSMutableArray *photosArray;

@end

@implementation ABPhotoCVC

static NSString *ownerId = @"-31255180";

static NSString * const reuseIdentifier = @"CellPhoto";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.photosArray = [[NSMutableArray alloc] init];
    
    self.navigationItem.title = self.photoAlbum.titlePhotosAlbum;
    
    [self getPhotoFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.photosArray count];
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ABPhotoCell *cell = (ABPhotoCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    ABPhoto *photo = [self.photosArray objectAtIndex:indexPath.row];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:photo.photo130];
    
    __weak ABPhotoCell *weakCell = cell;
    
    cell.imageViewCell.image = nil;
    
    [cell.imageViewCell setImageWithURLRequest:request
                              placeholderImage:nil
                                       success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                           weakCell.imageViewCell.image = image;
                                           [weakCell layoutSubviews];
                                       } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                           
                                       }];
    
    return cell;
}

- (void)getPhotoFromServer {
    
    [[ABServerManager shareManager]getPhotosOwnerId:ownerId albumId:self.photoAlbum.albumId count:self.photoAlbum.sizePhotos offset:0 onSuccess:^(NSArray *result) {
        
        if ([result count] > 0) {
            
            [self.photosArray addObjectsFromArray:result];
            
            NSMutableArray *newPaths = [NSMutableArray array];
            
            for (int i = (int)[self.photosArray count] - (int)[result count]; i < [self.photosArray count]; i++) {
                
                [newPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
            }
            
            [self.collectionView insertItemsAtIndexPaths:newPaths];
            
            [self.collectionView reloadData];
            
        }
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}


#pragma mark - ABAddPostDelegate

- (void)refreshgetPhotoFromServer {
    
    [[ABServerManager shareManager]getPhotosOwnerId:ownerId albumId:self.photoAlbum.albumId count:[self.photosArray count]+3 offset:0 onSuccess:^(NSArray *result) {
        
        if ([result count] > 0) {
            
            [self.photosArray removeAllObjects];
            
            [self.photosArray addObjectsFromArray:result];

            [self.collectionView reloadData];
        }
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

- (void)updateRequest {
    
    [self refreshgetPhotoFromServer];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"showDetailsPhoto"]) {
        
        NSIndexPath *selectedIndexPath = [self.collectionView indexPathsForSelectedItems][0];
        
        ABPhoto *photo = [self.photosArray objectAtIndex:selectedIndexPath.row];
        
        UINavigationController *nav = segue.destinationViewController;
        ABDetailsPhotoVC *detailsPhotoVC = (ABDetailsPhotoVC *)nav.topViewController;
        
        detailsPhotoVC.photo = photo;
        
    } else  if ([[segue identifier] isEqualToString:@"showPhotoForLoad"]) {
        
        UINavigationController *nav = segue.destinationViewController;
        ABCameraVC *cameraVC = (ABCameraVC *)nav.topViewController;
        cameraVC.photoAlbum = self.photoAlbum;
        cameraVC.delegate = self;
    }
    
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

- (IBAction)backAction:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
