//
//  ABPhoto.m
//  APIDZ-46
//
//  Created by tarik on 06.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABPhoto.h"

@implementation ABPhoto

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super initWithServerResponse:responseObject];
    if (self) {
        
        NSString *photo_130 = [responseObject objectForKey:@"photo_130"];
        
        if (photo_130) {
            self.photo130 = [NSURL URLWithString:photo_130];
        }
        
        NSString *photo_604 = [responseObject objectForKey:@"photo_604"];
        
        if (photo_604) {
            self.photo604 = [NSURL URLWithString:photo_604];
        }
        
        NSDictionary *likes = [responseObject objectForKey:@"likes"];
        
        self.countLikes = [[likes objectForKey:@"count"] integerValue];
        
        self.text = [responseObject objectForKey:@"text"];
        
        NSTimeInterval unixtime = [[responseObject objectForKey:@"updated"] doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixtime];
        
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd.MM.yyyy HH:mm"];
        
        self.date = [formatter stringFromDate:date];
        
    }
    return self;
}

@end

