//
//  ABCameraVC.h
//  APIDZ-46
//
//  Created by Александр on 14.11.15.
//  Copyright © 2015 Alex Bukharov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABCameraDelegate;

@class ABPhotoAlbum;

@interface ABCameraVC : UIViewController

@property (weak, nonatomic) id <ABCameraDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *savePhotoButton;

@property (strong, nonatomic) ABPhotoAlbum *photoAlbum;

- (IBAction)backAction:(UIBarButtonItem *)sender;

- (IBAction)CameraAction:(UIBarButtonItem *)sender;

- (IBAction)folderAction:(UIBarButtonItem *)sender;

- (IBAction)savePhotoAction:(UIBarButtonItem *)sender;


@end

@protocol ABCameraDelegate <NSObject>

- (void)updateRequest;

@end