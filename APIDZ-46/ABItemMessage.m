//
//  ABItemMessage.m
//  APIDZ-46
//
//  Created by tarik on 09.09.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABItemMessage.h"

@implementation ABItemMessage

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super initWithServerResponse:responseObject];
    if (self) {
        
        NSTimeInterval unixtime = [[responseObject objectForKey:@"date"] doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixtime];
        
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd.MM.yyyy hh:mm"];
        
        //self.date = [formatter stringFromDate:date];
        self.date = date;
        
        self.body = [responseObject objectForKey:@"body"];
        
        self.userId = [responseObject objectForKey:@"user_id"];
        
    }
    return self;
}

@end
