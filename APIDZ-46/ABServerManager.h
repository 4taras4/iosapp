//
//  ABServerManager.h
//  APIDZ-46
//
//  Created by tarik on 27.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ABUser;

@interface ABServerManager : NSObject

@property (strong, nonatomic) NSURL *avatarUser;

+ (ABServerManager *)shareManager;

- (void)authorizeUser:(void(^)(ABUser *user))completion;

- (void) getUser:(NSString *)userID
       onSuccess:(void(^)(ABUser *user))success
       onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)getWallWithUserId:(NSString *)ownerId
                   offset:(NSInteger)offset
                    count:(NSInteger)count
                onSuccess:(void (^)(id result))success
                onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

- (void) postWallOwnerId:(NSString *)ownerId
                 message:(NSString *)message
               onSuccess:(void(^)(id result))success
               onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)sendMessageForUserId:(NSString *)userId
                     message:(NSString *)message
                   onSuccess:(void(^)(id result))success
                   onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)getHistoryMessagesFromUserId:(NSString *)userId
                          senderName:(NSString*)senderName
                               count:(NSInteger)count
                              offset:(NSInteger)offset
                           onSuccess:(void(^)(NSArray *array))success
                           onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;


- (void)getCommentsFromPostId:(NSString *)postId
                      ownerId:(NSString *)ownerId
                        count:(NSInteger)count
                       offset:(NSInteger)offset
                    onSuccess:(void(^)(NSArray *array))success
                    onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)addCommentFromPostId:(NSString *)postId
                     ownerId:(NSString *)ownerId
                        text:(NSString *)text
                   onSuccess:(void(^)(NSArray *array))success
                   onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)deleteCommentFromPostId:(NSString *)commentId
                        ownerId:(NSString *)ownerId
                      onSuccess:(void(^)(NSArray *array))success
                      onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)addLikeFromObjectType:(NSString *)type
                      ownerId:(NSString *)ownerId
                       itemId:(NSString *)itemId
                    onSuccess:(void(^)(NSArray *array))success
                    onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)getWallById:(NSString *)posts
          onSuccess:(void (^)(id result))success
          onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

- (void)getVideoFromOwnerId:(NSString *)ownerId
                     videos:(NSString *)videos
                    albumId:(NSString *)albumId
                      count:(NSInteger)count
                  onSuccess:(void (^)(NSArray * result))success
                  onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

- (void)getVideoAlbumsOwnerId:(NSString *)ownerId
                       offset:(NSInteger)offset
                        count:(NSInteger)count
                    onSuccess:(void (^)(NSArray *result))success
                    onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

- (void)getPhotosAlbumsOwnerId:(NSString *)ownerId
                        offset:(NSInteger)offset
                         count:(NSInteger)count
                     onSuccess:(void (^)(NSArray *result))success
                     onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

- (void)getPhotosOwnerId:(NSString *)ownerId
                 albumId:(NSString *)albumId
                   count:(NSInteger)count
                  offset:(NSInteger)offset
               onSuccess:(void (^)(NSArray *result))success
               onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

- (void)getPhotosUploadServerFromGroupId:(NSString *)groupId
                                 albumId:(NSString *)albumId
                               onSuccess:(void (^)(id result))success
                               onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

- (void)postPhotoUploadUrl:(NSString *)uploadUrl
                 imageData:(NSData *)imageData
                 onSuccess:(void (^)(id result))success
                 onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

- (void)savePhotosForAlbumId:(NSString *)albumId
                     groupId:(NSString *)groupId
                      server:(NSString *)server
                  photosList:(NSString *)photosList
                        hash:(NSString *)hash
                   onSuccess:(void (^)(id result))success
                   onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

@end

