//
//  videoAlbum.m
//  APIDZ-46
//
//  Created by tarik on 05.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABVideoAlbum.h"

@implementation ABVideoAlbum

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super initWithServerResponse:responseObject];
    if (self) {
        
        
        self.countVideo = [[responseObject objectForKey:@"count"] integerValue];
        
        self.albumId = [responseObject objectForKey:@"id"];
        
        NSString *image = [responseObject objectForKey:@"photo_320"];
        
        if (image) {
            self.photo320 = [NSURL URLWithString:image];
        }
        
        self.titleVideo = [responseObject objectForKey:@"title"];
        
        NSTimeInterval unixtime = [[responseObject objectForKey:@"date"] doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixtime];
        
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd.MM.yyyy HH:mm"];
        
        self.dateUpdated = [formatter stringFromDate:date];
        
    }
    return self;
}

@end
