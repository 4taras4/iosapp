//
//  ABWall.h
//  APIDZ-46
//
//  Created by tarik on 27.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABServerObject.h"

@class ABUser;
@class ABGroup;

@interface ABWall : ABServerObject


@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *text;
@property (assign, nonatomic) NSInteger likesCount;
@property (assign, nonatomic) NSInteger commentsCount;
@property (strong, nonatomic) NSURL *photo;

@property (strong, nonatomic) NSString *fromId;
@property (strong, nonatomic) NSString *vid;
@property (strong, nonatomic) NSString *postId;
@property (strong, nonatomic) ABUser *user;
@property (strong, nonatomic) ABGroup *group;
@property (strong, nonatomic) NSArray *users;
@property (strong, nonatomic) NSString *type;



@end
