//
//  ABPlayer.m
//  APIDZ-46
//
//  Created by tarik on 28.10.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABVideo.h"

@implementation ABVideo

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super initWithServerResponse:responseObject];
    if (self) {

        self.player = [responseObject objectForKey:@"player"];
        
        NSString *image = [responseObject objectForKey:@"photo_320"];
        
        if (image) {
            self.photo320 = [NSURL URLWithString:image];
        }
        
        self.titleVideo = [responseObject objectForKey:@"title"];
        
        NSTimeInterval unixtime = [[responseObject objectForKey:@"date"] doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixtime];
        
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd.MM.yyyy HH:mm"];
        
        self.dateUpdated = [formatter stringFromDate:date];

    }
    
    return self;
}

@end


