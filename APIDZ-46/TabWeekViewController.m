//
//  TabWeekViewController.m
//  Today
//
//  Created by tarik on 15.03.16.
//  Copyright © 2016 tarik. All rights reserved.
//

#import "TabWeekViewController.h"
#import <XCDYouTubeVideoPlayerViewController.h>
#import "ABYoutubeVC.h"
#import <HCYoutubeParser.h>
#import <SWRevealViewController.h>
#import "ABAuthorizationVC.h"
#import <Masonry.h>
#import <EVTTabPageScrollView.h>
#import "JSONModel.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "CheduleViewCell.h"
#import "showDetailsteacherViewController.h"
#import "CheduleModel.h"
#import "JSONModel.h"





@interface TabWeekViewController ()

@property (strong, nonatomic) NSArray<CheduleModel *>* loans;

@property (nonatomic,strong) EVTTabPageScrollView *pageScroll;
@property (weak, nonatomic) UITableView* tableView;
@property (strong, nonatomic) NSArray *arrayFromAFNetworking;
@property (strong, nonatomic) NSMutableArray *responseData;

@end

@implementation TabWeekViewController

@synthesize weekSegmentedControl;


- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect frame = self.view.bounds;
    frame.origin = CGPointZero;
  
    UITableView* tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    UIBarButtonItem* editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                                target:self
                                                                                action:@selector(actionEdit:)];
    self.navigationItem.rightBarButtonItem = editButton;
    //self.arrayFromAFNetworking = [NSMutableArray array];
    
    
    [self makeRequests];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController) {
        [self.sidebarButton setTarget:self.revealViewController];
        [self.sidebarButton setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -  SegmentControllUI
- (IBAction)decodeButton:(id)sender {
    
    if (weekSegmentedControl.selectedSegmentIndex == 0) {
          }
    else {
       
    }
}


#pragma mark - JSON PArcing&Sorting


-(void)makeRequests{
    
    
    NSMutableArray *responceRequest=[[NSMutableArray alloc]init];
    
    NSURL *url = [NSURL URLWithString:@"http://eweb.in.ua/generated1.json"];
    //NSDictionary *parameters = @{@"group_name":@"11_234"};

   
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
   

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request ];
    
    
   
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation  setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        self.arrayFromAFNetworking = [responseObject objectForKey:@"lessons"];
        

        NSArray *weekSorting = [[NSSortDescriptor alloc] initWithKey:@"week" ascending:YES];
        NSArray *sortWeeks = [NSArray arrayWithObject:weekSorting];
        NSArray *sortedArrayWeeks = [_arrayFromAFNetworking sortedArrayUsingDescriptors:sortWeeks];
        NSArray *daySorting = [[NSSortDescriptor alloc] initWithKey:@"day" ascending:YES];
        NSArray *sortDays = [NSArray arrayWithObject:daySorting];
        NSArray *sortedArrayDays = [_arrayFromAFNetworking sortedArrayUsingDescriptors:sortDays];
     

        
        NSMutableArray *mySortedArray = [NSMutableArray arrayWithObjects:
                                          sortedArrayDays,nil];
          //NSMutableArray *index0 = [self.arrayFromAFNetworking valueForKey:@"week"];
        NSLog(@"Lessons loaded: %@",sortedArrayDays);
      
        [self.tableView reloadData];
       
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
 
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Перевірте інтернет зєднання"
                                                            message:@"та повторіть спробу."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
       
        [alertView show];
        [self.tableView reloadData];
        NSLog(@"Request Failed: %@, %@", error, error.userInfo);
        
    }];
    //operation.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
   
    [operation start];
    
    
}


- (void) actionEdit:(UIBarButtonItem*) sender {
    
    BOOL isEditing = self.tableView.editing;
    
    [self.tableView setEditing:!isEditing animated:YES];
    
    UIBarButtonSystemItem item = UIBarButtonSystemItemEdit;
    
    if (self.tableView.editing) {
        item = UIBarButtonSystemItemDone;
    }
   
    
    UIBarButtonItem* editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:item
                                                                                target:self
                                                                                action:@selector(actionEdit:)];
    [self.navigationItem setRightBarButtonItem:editButton animated:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
      return [_arrayFromAFNetworking count];
    NSLog(@"gheader sorted: %@", self.arrayFromAFNetworking);
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
 
    NSArray *daySorting = [[NSSortDescriptor alloc] initWithKey:@"day" ascending:YES];
    NSArray *sortDays = [NSArray arrayWithObject:daySorting];
    NSArray *sortedArrayDays = [_arrayFromAFNetworking sortedArrayUsingDescriptors:sortDays];
   // NSDictionary *arrayDaysNormal = [self.arrayFromAFNetworking setValue:@"1" forKey:@"day"];
 

    
    

    return [NSString stringWithFormat:@"%@",[[sortedArrayDays objectAtIndex:section] objectForKey:@"day"]];
     NSLog(@"Title sorted: %@", _arrayFromAFNetworking);
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return indexPath.row > 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  
        NSArray *tempOne = [self.arrayFromAFNetworking objectAtIndex:section ];
    
       // NSDictionary *eventsOnThisDay = [self.arrayFromAFNetworking objectForKey:@"week" ];
        return [tempOne count];

}



- (UITableViewCell *)tableView:(UITableView *)tableView  cellForRowAtIndexPath:(NSIndexPath *)indexPath
{      static NSString *CellIdentifier = @"cell";
    
    CheduleViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier  forIndexPath:indexPath];
    
      NSDictionary *tempDictionary = [self.arrayFromAFNetworking objectAtIndex:indexPath.row];
    NSDictionary *positionSorting = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    NSDictionary *positionSort = [NSArray arrayWithObject:positionSorting];
    NSDictionary *sortedArrayPosition = [[self.arrayFromAFNetworking sortedArrayUsingDescriptors:positionSort ] objectAtIndex:indexPath.row];

 
    cell.typeView.text = [tempDictionary objectForKey:@"type"];
    cell.teacherView.text = [tempDictionary objectForKey:@"teacher"];
    cell.subjectView.text = [tempDictionary objectForKey:@"subject"];
    cell.roomView.text = [NSString stringWithFormat:@"%@", [tempDictionary objectForKey:@"room"] ];
    cell.positionView.text = [NSString stringWithFormat:@"%@", [tempDictionary objectForKey:@"position"]];
    
   
    return cell;
}


#pragma mark - Prepare For Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    showDetailsteacherViewController *detailViewController = (UIViewController *)segue.destinationViewController;
    detailViewController.lessonsDetail = [self.arrayFromAFNetworking objectAtIndex:indexPath.row];
}


- (void)viewDidUnload {
    [super viewDidUnload];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation


@end
