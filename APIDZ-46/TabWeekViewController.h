//
//  TabWeekViewController.h
//  Today
//
//  Created by tarik on 15.03.16.
//  Copyright © 2016 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "ABSidebarTableViewController.h"
#import <SWRevealViewController.h>
#import "AFNetworking.h"
#import "CellsDetails.h"


@interface TabWeekViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate> /*{
    IBOutlet UITableView *tableViewChedule;
    NSArray *JsonArray;
    NSMutableData *data;
   
 
}
@property (strong, nonatomic) IBOutlet UITableView *tableViewChedule;
@property (weak, nonatomic) IBOutlet UILabel *textSubj;

@property (weak, nonatomic) IBOutlet UILabel *subj;
@property (strong, nonatomic) NSArray *googlePlacesArrayFromAFNetworking;
@property (strong, nonatomic) NSArray *finishedGooglePlacesArray;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, retain) NSArray *movies;

@property (weak, nonatomic) UILabel *textLabel;
                                                                                                       
*/
@property (nonatomic, strong) UISegmentedControl *weekSegmentedControl;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

-(void) retrieveData;
-(void) makeRestaurantRequests;
@end
