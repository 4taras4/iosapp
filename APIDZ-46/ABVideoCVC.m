//
//  ABVideoCVC.m
//  APIDZ-46
//
//  Created by tarik on 05.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABVideoCVC.h"
#import "ABVideoAlbum.h"
#import "ABVideo.h"
#import "ABServerManager.h"
#import "ABVideoCell.h"
#import "UIImageView+AFNetworking.h"
#import <HCYoutubeParser.h>
#import "ABPlayerViewController.h"
#import "Masonry.h"

@interface ABVideoCVC ()

@property (strong, nonatomic) NSMutableArray *videoArray;

@end

@implementation ABVideoCVC

static NSString *ownerId = @"-31255180";


static NSString * const reuseIdentifier = @"CellVideo";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.videoArray = [[NSMutableArray alloc] init];
    
    self.navigationItem.title = self.videoAlbum.titleVideo;
    
    [self getVideoFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.videoArray count];
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ABVideoCell*cell = (ABVideoCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    ABVideo *video = [self.videoArray objectAtIndex:indexPath.row];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:video.photo320];
    
    __weak ABVideoCell *weakCell = cell;
    
    cell.imageViewCell.image = nil;
    
    [cell.imageViewCell setImageWithURLRequest:request
                              placeholderImage:nil
                                       success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                           weakCell.imageViewCell.image = image;
                                           [weakCell layoutSubviews];
                                       } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                           
                                       }];
    
    cell.titleLabel.text = [NSString stringWithFormat:@"%@", video.titleVideo];
    
    cell.dateLabel.text = [NSString stringWithFormat:@"%@", video.dateUpdated];
    
    return cell;
}

- (void)getVideoFromServer {
    
    [[ABServerManager shareManager]getVideoFromOwnerId:ownerId videos:@"" albumId:self.videoAlbum.albumId count:self.videoAlbum.countVideo onSuccess:^(id result) {
        
        if ([result count] > 0) {
            
            [self.videoArray addObjectsFromArray:result];
            
            NSMutableArray *newPaths = [NSMutableArray array];
            
            for (int i = (int)[self.videoArray count] - (int)[result count]; i < [self.videoArray count]; i++) {
                
                [newPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
            }
            
            [self.collectionView insertItemsAtIndexPaths:newPaths];
            
            [self.collectionView reloadData];
            
        }
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"showVideoPlayer"]) {
        
        NSIndexPath *selectedIndexPath = [self.collectionView indexPathsForSelectedItems][0];
        
        ABVideo *video = [self.videoArray objectAtIndex:selectedIndexPath.row];
        ABPlayerViewController *playerViewController = segue.destinationViewController;
        
        NSString *url = [NSString stringWithFormat:@"%@", video.player];
        
        NSDictionary *videos = [HCYoutubeParser h264videosWithYoutubeURL:[NSURL URLWithString:url]];
        NSString *urlString=[NSString stringWithFormat:@"%@",[videos objectForKey:@"medium"]];
        
        playerViewController.urlString = urlString;
        
    }
    
}

- (IBAction)backAction:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
