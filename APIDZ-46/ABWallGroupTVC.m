//
//  ViewController.m
//  APIDZ-46
//
//  Created by tarik on 27.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABWallGroupTVC.h"
#import "ABServerManager.h"
#import "ABWall.h"
#import "ABUser.h"
#import "ABGroup.h"
#import "UIImageView+AFNetworking.h"
#import "ABWallCell1.h"
#import "ABWallCell2.h"
#import "ABWallCell3.h"
#import "ABAddPostCell.h"
#import "ABAddPostVC.h"
#import "ABCommentsPostTVC.h"
#import "ABAddMessageTVC.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "ABPlayerViewController.h"
#import "ABVideo.h"
#import <XCDYouTubeVideoPlayerViewController.h>
#import "ABYoutubeVC.h"
#import <HCYoutubeParser.h>
#import <SWRevealViewController.h>
#import "ABAuthorizationVC.h"

//#import <AVKit/AVKit.h>
@import AVFoundation;
@import AVKit;





@interface ABWallGroupTVC () <ABAddPostDelegate, ABCommentsPostDelegate>

@property (strong, nonatomic) NSMutableArray *wallArray;

@property (assign, nonatomic) BOOL firstTimeApper;

@property (strong, nonatomic) NSOperation *currentOperation;

@property (strong, nonatomic) NSOperationQueue *queue;

@property (strong, nonatomic) UIActivityIndicatorView *spinner;

@property (assign,nonatomic) BOOL loadingData;

@property (strong, nonatomic) AVPlayer *player;

@property (strong, nonatomic) XCDYouTubeVideoPlayerViewController *videoPlayerViewController;


@end

@implementation ABWallGroupTVC

static NSInteger wallInRequest = 20;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.ownerId = @"-31255180";
    
    self.wallArray = [NSMutableArray array];
    
    [self getWallBackgroundFromServer];
    
    self.firstTimeApper = YES;
    
    self.loadingData = YES;
    
    [self infiniteScrolling];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if (revealViewController) {
        [self.sidebarButton setTarget:self.revealViewController];
        [self.sidebarButton setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)infiniteScrolling {
    
    __weak typeof(self) weakSelf = self;
    // refresh new data when pull the table list
    [self.tableView addPullToRefreshWithActionHandler:^{
        //weakSelf.currentPage = initialPage; // reset the page
        [weakSelf.wallArray removeAllObjects]; // remove all data
        [weakSelf.tableView reloadData]; // before load new content, clear the existing table list
        [weakSelf getWallBackgroundFromServer]; // load new data
        [weakSelf.tableView.pullToRefreshView stopAnimating]; // clear the animation
        
        // once refresh, allow the infinite scroll again
        weakSelf.tableView.showsInfiniteScrolling = YES;
    }];
    
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf getWallBackgroundFromServer];
    }];
}


- (void)refresh:(UIRefreshControl *)refreshControl {
    [self updateRequest];
    
    [refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    //self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if (self.firstTimeApper) {
        self.firstTimeApper = NO;
        
        [[ABServerManager shareManager] authorizeUser:^(ABUser *user) {
            
            self.avatarUser = user.imageURL;
            
            NSLog(@"AUTHORIZED");
            NSLog(@"%@ %@", user.lastName, user.firstName);
        }];
    }
}

- (void)getWallBackgroundFromServer {
    
    self.queue = [[NSOperationQueue alloc] init];
    
    __weak NSOperation *weakCurrentOperation = self.currentOperation;
    
    __weak ABWallGroupTVC* weakSelf = self;
    
    self.currentOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        if (![weakCurrentOperation isCancelled]) {
            
            [weakSelf getWallFromServer];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.currentOperation = nil;
        });
        
    }];
    
    [self.queue addOperation:self.currentOperation];
}

#pragma mark - API

- (void)getWallFromServer {
    
    [[ABServerManager shareManager] getWallWithUserId:self.ownerId offset:[self.wallArray count] count:wallInRequest onSuccess:^(NSArray *wall) {
        //NSLog(@"%ld", (unsigned long)[self.wallArray count]);
        if ([wall count] > 0) {
            
            [self.wallArray addObjectsFromArray:wall];
            
            NSMutableArray *newPaths = [NSMutableArray array];
            
            for (int i = (int)[self.wallArray count] - (int)[wall count]; i < [self.wallArray count]; i++) {
                
                [newPaths addObject:[NSIndexPath indexPathForItem:i inSection:1]];
            }
            
            [self.tableView insertRowsAtIndexPaths:newPaths withRowAnimation:UITableViewRowAnimationFade];
            
            //self.loadingData = NO;
            [self.tableView.pullToRefreshView stopAnimating];
            [self.tableView.infiniteScrollingView stopAnimating];
            

            
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        self.tableView.showsInfiniteScrolling = NO;
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return 1;
        
    } else {
    
        return [self.wallArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"ABWallCell1";
    
    static NSString *identifier2 = @"ABWallCell2";
    
    static NSString *identifier3 = @"ABAddMessageCell";
    
    static NSString *identifier4 = @"ABWallCell3";
    
    
    
    //ABWall *wall = [self.wallArray objectAtIndex:indexPath.row];
    
    if (indexPath.section == 0) {
        
        ABAddPostCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier3];
        
        return cell;
        
    } else {
        
        ABWall *wall = [self.wallArray objectAtIndex:indexPath.row];
        
        if ([wall.type isEqualToString:@"photo"]) {
            
            ABWallCell1 *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            if (wall.user.firstName || wall.user.lastName) {
                cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", wall.user.firstName, wall.user.lastName];
            } else {
                cell.nameLabel.text = wall.group.groupName;
            }
            
            cell.textWallLabel.text = wall.text;
            
            cell.dateLabel.text = wall.date;
            cell.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)wall.likesCount];
            cell.comments.text = [NSString stringWithFormat:@"%ld", (long)wall.commentsCount];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:wall.photo];
            
            __weak ABWallCell1 *weakCell = cell;
            
            cell.photoView.image = nil;
            
            [cell.photoView setImageWithURLRequest:request
                                  placeholderImage:nil
                                           success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                               weakCell.photoView.image = image;
                                               [weakCell layoutSubviews];
                                           } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                               
                                           }];
            
            NSURLRequest *requestAvatar = nil;
            
            if (wall.user.photoMediumURL) {
                requestAvatar = [NSURLRequest requestWithURL:wall.user.photoMediumURL];
            } else {
                requestAvatar = [NSURLRequest requestWithURL:wall.group.photoMediumURL];
            }
            
            __weak ABWallCell1 *weakAvatarCell = cell;
            
            cell.avatar.image = nil;
            
            [cell.avatar setImageWithURLRequest:requestAvatar
                               placeholderImage:nil
                                        success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                            weakAvatarCell.avatar.image = image;
                                            [weakAvatarCell layoutSubviews];
                                            CALayer *imageLayer = weakAvatarCell.avatar.layer;
                                            [imageLayer setCornerRadius:30];
                                            [imageLayer setMasksToBounds:YES];
                                        } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                            
                                        }];
            
            [self tapGestureForImageView:cell.avatar];
            [self tapGestureForImageViewPhoto:cell.photoView];
            [self tapGestureForLabel:cell.textWallLabel];
            
            
            return cell;
            
        } else if ([wall.type isEqualToString:@"video"]) {
            
            ABWallCell3 *cell = [tableView dequeueReusableCellWithIdentifier:identifier4];
            
            
            if (wall.user.firstName || wall.user.lastName) {
                cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", wall.user.firstName, wall.user.lastName];
            } else {
                cell.nameLabel.text = wall.group.groupName;
            }
            cell.textWallLabel.text = wall.text;
            cell.dateLabel.text = wall.date;
            cell.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)wall.likesCount];
            cell.comments.text = [NSString stringWithFormat:@"%ld", (long)wall.commentsCount];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:wall.photo];
            
            __weak ABWallCell3 *weakCell = cell;
            
            cell.photoView.image = nil;
            
            [cell.photoView setImageWithURLRequest:request
                                  placeholderImage:nil
                                           success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                               weakCell.photoView.image = image;
                                               [weakCell layoutSubviews];
                                           } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                               
                                           }];
            
            NSURLRequest *requestAvatar = nil;
            
            if (wall.user.photoMediumURL) {
                requestAvatar = [NSURLRequest requestWithURL:wall.user.photoMediumURL];
            } else {
                requestAvatar = [NSURLRequest requestWithURL:wall.group.photoMediumURL];
            }
            
            __weak ABWallCell3 *weakAvatarCell = cell;
            
            cell.avatar.image = nil;
            
            [cell.avatar setImageWithURLRequest:requestAvatar
                               placeholderImage:nil
                                        success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                            weakAvatarCell.avatar.image = image;
                                            [weakAvatarCell layoutSubviews];
                                            CALayer *imageLayer = weakAvatarCell.avatar.layer;
                                            [imageLayer setCornerRadius:30];
                                            [imageLayer setMasksToBounds:YES];
                                        } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                            
                                        }];
            
            [self tapGestureForImageView:cell.avatar];
            [self tapGestureForViewVideo:cell.photoView];
            [self tapGestureForLabel:cell.textWallLabel];
            

            return cell;
            
    
        } else {
            
            ABWallCell2 *cell = [tableView dequeueReusableCellWithIdentifier:identifier2];
            
            
            if (wall.user.firstName || wall.user.lastName) {
                cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", wall.user.firstName, wall.user.lastName];
            } else {
                cell.nameLabel.text = wall.group.groupName;
            }
            cell.textWallLabel.text = wall.text;
            cell.dateLabel.text = wall.date;
            cell.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)wall.likesCount];
            cell.comments.text = [NSString stringWithFormat:@"%ld", (long)wall.commentsCount];
            
            NSURLRequest *requestAvatar = nil;
            
            if (wall.user.photoMediumURL) {
                requestAvatar = [NSURLRequest requestWithURL:wall.user.photoMediumURL];
            } else {
                requestAvatar = [NSURLRequest requestWithURL:wall.group.photoMediumURL];
            }
            
            __weak ABWallCell2 *weakAvatarCell = cell;
            
            cell.avatar.image = nil;
            
            [cell.avatar setImageWithURLRequest:requestAvatar
                               placeholderImage:nil
                                        success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                            weakAvatarCell.avatar.image = image;
                                            [weakAvatarCell layoutSubviews];
                                            CALayer *imageLayer = weakAvatarCell.avatar.layer;
                                            [imageLayer setCornerRadius:30];
                                            [imageLayer setMasksToBounds:YES];
                                        } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                            
                                        }];
            
            [self tapGestureForImageView:cell.avatar];
            [self tapGestureForLabel:cell.textWallLabel];
            
            return cell;
        }
    }
 
    return nil;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.videoPlayerViewController.moviePlayer play];
}

- (void)tapGestureForImageView:(UIImageView *)imageView {
    
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapImageView:)];
    [imageView addGestureRecognizer:recognizer];
    
}

- (void)onTapImageView:(UITapGestureRecognizer *)recognizer {
    
    //touch recognition on imageView
    UIImageView *imageView = (UIImageView *)[recognizer view];
    
    UITableViewCell *cell = (UITableViewCell *)[[imageView superview]superview];

    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    ABWall *wall = [self.wallArray objectAtIndex:indexPath.row];

    ABAddMessageTVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ABAddMessageTVC"];
    
    vc.senderId = wall.fromId;

    vc.senderDisplayName = [NSString stringWithFormat:@"%@ %@", wall.user.firstName, wall.user.lastName];
    
    vc.avatarIncoming = wall.user.photo;
    
    vc.avatarOutgoing = [ABServerManager shareManager].avatarUser;//self.avatarUser;
    
    //NSLog(@"wall.user.photo-%@, self.avatarUser%@", wall.user.photo, vc.avatarOutgoing);
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)tapGestureForLabel:(UILabel *)label {
    
    label.userInteractionEnabled = YES;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapLabel:)];
    [label addGestureRecognizer:recognizer];
    
}

- (void)onTapLabel:(UITapGestureRecognizer *)recognizer {
    
    UILabel *label = (UILabel *)[recognizer view];
    
    UITableViewCell *cell = (UITableViewCell *)[[label superview]superview];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    ABWall *wall = [self.wallArray objectAtIndex:indexPath.row];
    
    ABCommentsPostTVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ABCommentsPostTVC"];
    
    vc.wall = wall;
    
    vc.delegate = self;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tapGestureForViewVideo:(UIView *)imageView {
    
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapViewVideo:)];
    [imageView addGestureRecognizer:recognizer];
    
}

- (void)onTapViewVideo:(UITapGestureRecognizer *)recognizer {
    
    UIView *imageView = (UIView *)[recognizer view];
    
    ABWallCell3 *cell = (ABWallCell3 *)[[imageView superview]superview];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    ABWall *wall = [self.wallArray objectAtIndex:indexPath.row];
    
    NSString *videos = [NSString stringWithFormat:@"%@_%@", wall.fromId, wall.vid];
    
    [[ABServerManager shareManager]getVideoFromOwnerId:wall.fromId videos:videos albumId:@"" count:1 onSuccess:^(NSArray *result) {
        
        if (result) {
            
            ABVideo *video = [result firstObject];
            
            NSString *url = [NSString stringWithFormat:@"%@", video.player];
            
            if (url) {
                NSArray *arrayBeginUrl = [url componentsSeparatedByString:@"/"];
                
                NSString *stringBeginUrl = [arrayBeginUrl objectAtIndex:2];
                
                NSLog(@"stringBeginUrl %@", stringBeginUrl);
                
                if ([stringBeginUrl isEqualToString:@"vk.com"]) {
                    NSLog(@"Problem play video VK");
                } else if ([stringBeginUrl isEqualToString:@"www.youtube.com"]) {
                    
                    NSDictionary *videos = [HCYoutubeParser h264videosWithYoutubeURL:[NSURL URLWithString:url]];
                    NSString *urlString=[NSString stringWithFormat:@"%@",[videos objectForKey:@"medium"]];
                    
                    ABPlayerViewController *pvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ABPlayerViewController"];
                    
                    pvc.urlString = urlString;
                    
                    [self presentViewController:pvc animated:YES completion:nil];
                    
                } else {
                    NSLog(@"Problem play video");
                }
            }
        }
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

- (void)tapGestureForImageViewPhoto:(UIImageView *)imageView {
    
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapImageViewPhoto:)];
    [imageView addGestureRecognizer:recognizer];
    
}

- (void)onTapImageViewPhoto:(UITapGestureRecognizer *)recognizer {
   
    UIImageView *imageView = (UIImageView *)[recognizer view];
    
    UITableViewCell *cell = (UITableViewCell *)[[imageView superview]superview];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    ABWall *wall = [self.wallArray objectAtIndex:indexPath.row];
    
    ABCommentsPostTVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ABCommentsPostTVC"];
    
    vc.wall = wall;
    
    vc.delegate = self;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}



- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    return UITableViewAutomaticDimension;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"AddPostSegue"]) {
        
        ABAddPostVC *vc = (ABAddPostVC *)[segue destinationViewController];
        vc.ownerId = self.ownerId;
        vc.delegate = self;
    }
   
}

#pragma mark - Supplementary methods

- (void)refreshWall {
    
    [[ABServerManager shareManager]getWallWithUserId:self.ownerId offset:0 count:MAX(10, [self.wallArray count]) onSuccess:^(NSArray *wall) {
        if ([wall count] > 0) {
            
            [self.wallArray removeAllObjects];
            
            [self.wallArray addObjectsFromArray:wall];
            
            [self.tableView reloadData];
            
            self.loadingData = NO;
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

#pragma mark - ABAddPostDelegate

- (void)updateRequest {
    
    [self refreshWall];
    
    [self delegate];
 
}

- (void)refreshPost {
    
    [self refreshWall];
}






@end
