//
//  ABVideoCVC.m
//  APIDZ-46
//
//  Created by tarik on 04.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABVideoAlbumCVC.h"
#import <SWRevealViewController.h>
#import "ABServerManager.h"
#import "ABVideoAlbum.h"
#import "ABAlbumCell.h"
#import "UIImageView+AFNetworking.h"
#import "ABVideoCVC.h"
#import "Masonry.h"

@interface ABVideoAlbumCVC ()


@property (strong, nonatomic) NSMutableArray *videoArray;


@end

@implementation ABVideoAlbumCVC

static NSInteger count = 20;
static NSInteger offset = 0;
static NSString *ownerId = @"-31255180";

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.videoArray = [[NSMutableArray alloc] init];
    
    self.navigationItem.title = @"test my self";
    
    [self getVideoAlbumsFromServer];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if (revealViewController) {
        [self.sidebarButton setTarget:self.revealViewController];
        [self.sidebarButton setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.videoArray count];
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ABAlbumCell *cell = (ABAlbumCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    ABVideoAlbum *videoAlbum = [self.videoArray objectAtIndex:indexPath.row];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:videoAlbum.photo320];
    
    __weak ABAlbumCell *weakCell = cell;
    
    cell.imageViewCell.image = nil;
    
    [cell.imageViewCell setImageWithURLRequest:request
                          placeholderImage:nil
                                   success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                       weakCell.imageViewCell.image = image;
                                       [weakCell layoutSubviews];
                                   } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                       
                                   }];
    
    cell.titleLabel.text = [NSString stringWithFormat:@"%@", videoAlbum.titleVideo];
    
    cell.dateLabel.text = [NSString stringWithFormat:@"%@", videoAlbum.dateUpdated];
 
    cell.countVideoLabel.text = [NSString stringWithFormat:@"%ld видеозаписи", (long)videoAlbum.countVideo];
    
    return cell;
}

- (void)getVideoAlbumsFromServer {
    
    [[ABServerManager shareManager] getVideoAlbumsOwnerId:ownerId offset:offset count:count onSuccess:^(NSArray *result) {
        
        if ([result count] > 0) {
            
            [self.videoArray addObjectsFromArray:result];
            
            NSMutableArray *newPaths = [NSMutableArray array];
            
            for (int i = (int)[self.videoArray count] - (int)[result count]; i < [self.videoArray count]; i++) {
                
                [newPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
            }
            
            [self.collectionView insertItemsAtIndexPaths:newPaths];
            
            [self.collectionView reloadData];
            
        }
        
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"showVideoCVC"]) {
        
        NSIndexPath *selectedIndexPath = [self.collectionView indexPathsForSelectedItems][0];
        
        ABVideoAlbum *videoAlbum = [self.videoArray objectAtIndex:selectedIndexPath.row];
        UINavigationController *nav = segue.destinationViewController;
        ABVideoCVC *videoCVC = (ABVideoCVC *)nav.topViewController;
        videoCVC.videoAlbum = videoAlbum;
    }
    
}


@end
