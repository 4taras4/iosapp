//
//  ABPhotoCell.h
//  APIDZ-46
//
//  Created by tarik on 06.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABPhotoCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewCell;
@property (nonatomic) BOOL checked;
@property (weak, nonatomic) IBOutlet UIImageView* checkBoxImageView;

@end
