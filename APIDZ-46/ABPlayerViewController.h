//
//  ABPlayerViewController.h
//  APIDZ-46
//
//  Created by tarik on 27.10.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>




@interface ABPlayerViewController : AVPlayerViewController

@property (strong, nonatomic) NSString *urlString;




//- (void)setupPlaybackWithURL:(NSURL *)movieURL;

@end
