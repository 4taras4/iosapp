//
//  ABDetailsPhotoVC.m
//  APIDZ-46
//
//  Created by tarik on 07.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABDetailsPhotoVC.h"
#import "ABPhoto.h"
#import "UIImageView+AFNetworking.h"
#import "Masonry.h"

@interface ABDetailsPhotoVC ()

@end

@implementation ABDetailsPhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.photoView.image =
    
    NSURLRequest *request = [NSURLRequest requestWithURL:self.photo.photo604];
    
    __weak ABDetailsPhotoVC *weakVC = self;
    
    self.photoView.image = nil;
    
    [self.photoView setImageWithURLRequest:request
                              placeholderImage:nil
                                       success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                           weakVC.photoView.image = image;
                                           //[weakCell layoutSubviews];
                                       } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                           
                                       }];
    self.titleLabel.text = @"";
    self.likesLabel.text = @"";

    /*
    self.titleLabel.text = self.photo.text;
    
    self.likesLabel.text = [NSString stringWithFormat:@"%ld", self.photo.countLikes];
    */
    [self tapGestureForImageView:self.photoView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tapGestureForImageView:(UIImageView *)imageView {
    
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapImageView:)];
    [imageView addGestureRecognizer:recognizer];
    
}

- (void)onTapImageView:(UITapGestureRecognizer *)recognizer {
    
    if ([self.titleLabel.text isEqualToString:@""]) {
        self.titleLabel.text = self.photo.text;
    } else {
        self.titleLabel.text = @"";
    }
    if ([self.likesLabel.text isEqualToString:@""]) {
        self.likesLabel.text = [NSString stringWithFormat:@"%ld", (long)self.photo.countLikes];
        self.lilesImageView.image = [UIImage imageNamed:@"heart-empty-icon.png"];
        
    } else {
        self.likesLabel.text = @"";
        self.lilesImageView.image = [UIImage imageNamed:@""];
    }
    
    
}


- (IBAction)backAction:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


@end
