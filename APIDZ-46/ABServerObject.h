//
//  ABServerObject.h
//  APIDZ-46
//
//  Created by tarik on 17.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABServerObject : NSObject

- (id)initWithServerResponse:(NSDictionary *)responseObject;

@end
