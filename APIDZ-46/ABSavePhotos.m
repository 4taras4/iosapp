//
//  ABSavePhotos.m
//  APIDZ-46
//
//  Created by tarik on 13.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABSavePhotos.h"

@implementation ABSavePhotos

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super initWithServerResponse:responseObject];
    if (self) {
        
        self.albumId = [responseObject objectForKey:@"aid"];
        self.groupId = [responseObject objectForKey:@"gid"];
        self.hashPhoto = [responseObject objectForKey:@"hash"];
        self.photosList = [responseObject objectForKey:@"photos_list"];
        self.server = [responseObject objectForKey:@"server"];
        
        
    }
    return self;
}

@end
