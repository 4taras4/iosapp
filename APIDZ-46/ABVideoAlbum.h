//
//  videoAlbum.h
//  APIDZ-46
//
//  Created by tarik on 05.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABServerObject.h"

@interface ABVideoAlbum : ABServerObject

@property (strong, nonatomic) NSURL *photo320;
@property (strong, nonatomic) NSString *titleVideo;
@property (assign, nonatomic) NSInteger countVideo;
@property (strong, nonatomic) NSString *dateUpdated;
@property (strong, nonatomic) NSString *albumId;

@end
