//
//  ABMessageVC.m
//  APIDZ-46
//
//  Created by tarik on 30.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABAddPostVC.h"
#import "ABServerManager.h"

@interface ABAddPostVC () <UITextViewDelegate>

@end

@implementation ABAddPostVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([self.textMessage.text isEqualToString:@""]) {
        self.saveButton.enabled = NO;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)postWallFromServer {
    
    [[ABServerManager shareManager] postWallOwnerId:self.ownerId
                                            message:self.textMessage.text
                                          onSuccess:^(id result) {
                                              
                                              [self.navigationController popViewControllerAnimated:YES];
                                              [self.delegate updateRequest];
                                              
                                          } onFailure:^(NSError *error, NSInteger statusCode) {
                                              NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
                                          }];
     
    

}

- (IBAction)cancelAction:(UIBarButtonItem *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)saveAction:(UIBarButtonItem *)sender {
    
    [self postWallFromServer];
    /*
    [self.delegate updateRequest];
    
    [self.navigationController popViewControllerAnimated:YES];
    */
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView{
    
    if ([self.textMessage.text length] > 0) {
        self.saveButton.enabled = YES;
    } else {
        self.saveButton.enabled = NO;
    }
    
    //self.saveButton.enabled = YES;
    
}

@end
