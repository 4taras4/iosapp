//
//  ABItemMessage.h
//  APIDZ-46
//
//  Created by tarik on 09.09.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABServerObject.h"

@interface ABItemMessage : ABServerObject

@property (strong, nonatomic) NSString *body;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSDate *date;


@end
