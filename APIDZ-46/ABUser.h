//
//  ABUser.h
//  APIDZ-46
//
//  Created by tarik on 27.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABServerObject.h"

@interface ABUser : ABServerObject

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSURL *imageURL;
@property (strong, nonatomic) NSURL *photo;
@property (strong, nonatomic) NSURL *photoMediumURL;


@end
