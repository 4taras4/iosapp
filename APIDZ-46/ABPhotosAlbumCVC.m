//
//  ABPhotosAlbum.m
//  APIDZ-46
//
//  Created by tarik on 06.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABPhotosAlbumCVC.h"
#import "ABServerManager.h"
#import <SWRevealViewController.h>
#import "ABPhotosAlbumCell.h"
#import "ABPhotoAlbum.h"
#import "ABPhotoCVC.h"
#import "Masonry.h"


@interface ABPhotosAlbumCVC ()

@property (strong, nonatomic) NSMutableArray *photosAlbumArray;

@end

@implementation ABPhotosAlbumCVC

static NSString *ownerId = @"-31255180";

static NSString * const reuseIdentifier = @"CellPhotoAlbum";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.photosAlbumArray = [[NSMutableArray alloc] init];
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if (revealViewController) {
        [self.sidebarButton setTarget:self.revealViewController];
        [self.sidebarButton setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    self.navigationItem.title = @"Фотоальбомы";
    
    [self getPhotosAlbumFromServer];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.photosAlbumArray count];
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ABPhotosAlbumCell *cell = (ABPhotosAlbumCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    ABPhotoAlbum *photoAlbum = [self.photosAlbumArray objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = [NSString stringWithFormat:@"%@", photoAlbum.titlePhotosAlbum];
    
    cell.dateLabel.text = [NSString stringWithFormat:@"%@", photoAlbum.dateUpdated];
    
    cell.countPhotosLabel.text = [NSString stringWithFormat:@"%ld фотографий", (long)photoAlbum.sizePhotos];
    
    return cell;
}

- (void)getPhotosAlbumFromServer {
    
    [[ABServerManager shareManager]getPhotosAlbumsOwnerId:ownerId offset:0 count:(long)NULL onSuccess:^(NSArray *result) {
        
        if ([result count] > 0) {
            
            [self.photosAlbumArray addObjectsFromArray:result];
            
            NSMutableArray *newPaths = [NSMutableArray array];
            
            for (int i = (int)[self.photosAlbumArray count] - (int)[result count]; i < [self.photosAlbumArray count]; i++) {
                
                [newPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
            }
            
            [self.collectionView insertItemsAtIndexPaths:newPaths];
            
            [self.collectionView reloadData];
            
        }
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"showPhotos"]) {
        
        NSIndexPath *selectedIndexPath = [self.collectionView indexPathsForSelectedItems][0];
        
        ABPhotoAlbum *photoAlbum = [self.photosAlbumArray objectAtIndex:selectedIndexPath.row];
        UINavigationController *nav = segue.destinationViewController;
        ABPhotoCVC *photoCVC = (ABPhotoCVC *)nav.topViewController;
        photoCVC.photoAlbum = photoAlbum;
    }
    
}

@end
